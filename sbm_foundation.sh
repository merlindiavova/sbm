#!/bin/sh
# shellcheck disable=SC1090

# Name:        sbm_foundation.sh
# Version:     0.0.5
# Description: Shared logic for all SBM utilities. Source this file only.
# Maintainer:  Merlin Diavova <merlindiavova@pm.me>
# Author:      Merlin Diavova
#
# Copyright (c) 2020 Merlin Diavova <merlindiavova@pm.me>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -eu

[ -n "${TRACE+defined}" ] && set -x

if [ -z "${SBM_BASE_PATH:-}" ];
then
  a="/$0"; a=${a%/*}; a=${a#/}; a=${a:-.}
  SBM_BASE_PATH=$(cd "$a" || return; pwd)
  unset a
fi

SBM_E_INVALID_ARGUMENTS_COUNT=10
SBM_E_INVALID_ARGUMENTS=11
SBM_E_UNDEFINED_VALUE=12
SBM_E_WRITE_FAILED=20
SBM_E_NOT_FOUND=40
SBM_E_OPEN_HANDLER_NOT_FOUND=41
SBM_E_FINDER_NOT_FOUND=42
SBM_E_BROWSER_NOT_FOUND=43
SBM_E_BOOKMARK_EXISTS=50

SBM_CONFIG_PATH=${XDG_CONFIG_HOME:="$HOME/.config"}/sbm
SBM_DATA_PATH=${XDG_DATA_HOME:="$HOME/.local/share"}/sbm
SBM_CONFIG_FILE=${SBM_CONFIG_PATH}/config
SBM_VERBOSE_LEVEL=${SBM_VERBOSE_LEVEL:=0}
SBM_HELP_TAB_LENGTH=28

readonly SBM_BASE_PATH
readonly SBM_E_INVALID_ARGUMENTS_COUNT SBM_E_INVALID_ARGUMENTS
readonly SBM_E_WRITE_FAILED SBM_E_UNDEFINED_VALUE
readonly SBM_E_NOT_FOUND SBM_E_OPEN_HANDLER_NOT_FOUND
readonly SBM_E_FINDER_NOT_FOUND SBM_E_BROWSER_NOT_FOUND
readonly SBM_E_BOOKMARK_EXISTS SBM_CONFIG_PATH SBM_DATA_PATH
readonly SBM_HELP_TAB_LENGTH

export SBM_BASE_PATH
export SBM_E_INVALID_ARGUMENTS_COUNT SBM_E_INVALID_ARGUMENTS
export SBM_E_UNDEFINED_VALUE SBM_E_NOT_FOUND
export SBM_E_OPEN_HANDLER_NOT_FOUND SBM_E_FINDER_NOT_FOUND
export SBM_E_BROWSER_NOT_FOUND SBM_E_BOOKMARK_EXISTS
export SBM_CONFIG_PATH SBM_DATA_PATH SBM_HELP_TAB_LENGTH

sbm_split_string() {
  set -f

  old_ifs=$IFS
  IFS=$2
  # shellcheck disable=2086
  set -- $1

  command printf '%s\n' "$@"

  IFS=$old_ifs
  set +f
}

sbm_split_argument() {
  set -f

  old_ifs=$IFS
  IFS=$2
  # shellcheck disable=2086
  set -- $1

  command printf '%s ' "$@"

  IFS=$old_ifs
  set +f
}

sbm_trim_all() {
    # Usage: trim_all "   example   string    "

    # Disable globbing to make the word-splitting below safe.
    set -f

    # Set the argument list to the word-splitted string.
    # This removes all leading/trailing white-space and reduces
    # all instances of multiple spaces to a single ("  " -> " ").
    # shellcheck disable=SC2048
    # shellcheck disable=SC2086
    set -- $*

    # Print the argument list as a string.
    printf '%s\n' "$*"

    # Re-enable globbing.
    set +f
}

sbm_lstrip() {
  command printf '%s\n' "${1##$2}"
}

sbm_rstrip() {
  command printf '%s\n' "${1%%$2}"
}

sbm_check_program_exists() {
  command -v "$1" >/dev/null 2>&1
}

sbm_dirname() {
    # Usage: dirname "path"

    # If '$1' is empty set 'dir' to '.', else '$1'.
    dir=${1:-.}

    # Strip all trailing forward-slashes '/' from
    # the end of the string.
    #
    # "${dir##*[!/]}": Remove all non-forward-slashes
    # from the start of the string, leaving us with only
    # the trailing slashes.
    # "${dir%%"${}"}:  Remove the result of the above
    # substitution (a string of forward slashes) from the
    # end of the original string.
    dir=${dir%%"${dir##*[!/]}"}

    # If the variable *does not* contain any forward slashes
    # set its value to '.'.
    [ "${dir##*/*}" ] && dir=.

    # Remove everything *after* the last forward-slash '/'.
    dir=${dir%/*}

    # Again, strip all trailing forward-slashes '/' from
    # the end of the string (see above).
    dir=${dir%%"${dir##*[!/]}"}

    # Print the resulting string and if it is empty,
    # print '/'.
    printf '%s\n' "${dir:-/}"
}

sbm_resolve_path() {
  # shellcheck disable=SC2088
  case "$1" in
    '~/'*) command echo "${HOME}/$(sbm_lstrip "$1" '~/')"; return ;;
    *) command echo "$1"; return ;;
  esac
}

sbm_revert_path() {
  # shellcheck disable=SC2088
  case "$1" in
    '/home/'*)
      command printf '%s%s\n' '~' \
        "$(sbm_lstrip "$1" "/home/$(id -u -n)")"; return ;;
    *) command echo "$1"; return ;;
  esac
}

sbm_check_path_exists() {
  [ -e "$(sbm_resolve_path "$1")" ]
}

sbm_left_trim() {
  sed -e "s/^$1//" | sed -e "s/[[:space:]]*//"
}

sbm_read_self_doc_value() {
  command printf '%s' \
    "$(grep -m1 -E "^# $1:" "$0" | sbm_left_trim "# $1:")"
}

sbm_notify() {
  if command -v notify-send >/dev/null 2>&1;
  then
    return 1
  fi

  notify-send "$1" "$2"
}

# shellcheck disable=SC2120
sbm_get_path() {
  if [ "$#" -ge 1 ];
  then
    sbm_lstrip "$1" "*$SBM_OPTION_SEPARATOR"
  else
    while read -r line ;
    do
      sbm_lstrip "$line" "*$SBM_OPTION_SEPARATOR"
    done
    unset line
  fi
}

# shellcheck disable=SC2120
sbm_get_label() {
  if [ "$#" -ge 1 ];
  then
    sbm_rstrip "$1" "$SBM_OPTION_SEPARATOR*"
  else
    while read -r line ;
    do
      sbm_rstrip "$line" "*$SBM_OPTION_SEPARATOR*"
    done
    unset line
  fi
}

sbm_print_line() {
  command printf '%s\n' "$1"
}

sbm_print() {
  message="$1"
  colour_code="${2:-\033[0;37m}";
  print_reset_colour='\033[0m'

  command echo "${colour_code}${message}${print_reset_colour}"  2>/dev/null
  unset message colour_code print_reset_colour
}

sbm_print_blue() {
  sbm_print "$@" '\033[0;34m'
}

sbm_print_bold() {
  sbm_print "$@" '\033[1;37m'
}

sbm_print_bold_yellow() {
  sbm_print "$@" '\033[0;33m'
}

sbm_print_green() {
  sbm_print "$@" '\033[0;32m'
}

sbm_print_red() {
  sbm_print "$@" '\033[0;91m'
}

sbm_print_yellow() {
  sbm_print "$@" '\033[0;93m'
}

sbm_print_info() {
  if [ "$SBM_VERBOSE_LEVEL" -ge 1 ];
  then
    sbm_print "$(sbm_print_blue "[Info]")       $1"
  fi
}

sbm_print_success() {
  sbm_print "$(sbm_print_green "[Success]")    $1"
}

sbm_bookmark_exists() {
  grep -qm1 "$2" "$1" || grep -qm1 "$3" "$1"
}

sbm_print_error_message() {
  error_code="$1"; shift;

  error_message=''
  extra_message=''

  if  [ -n "$*" ];
  then
    extra_message="${*}. "
  fi

  case "${error_code}" in
    # SBM_E_INVALID_ARGUMENTS_COUNT
    10) error_message='Invalid number of arguments given.' ;;
    # SBM_E_INVALID_ARGUMENTS
    11) error_message='Invalid arguments supplied' ;;
    # SBM_E_UNDEFINED_VALUE
    12) error_message='Undefined value.' ;;
    # SBM_E_WRITE_FAILED
    20) error_message='Failed to write resource.' ;;
    # SBM_E_NOT_FOUND
    40) error_message='Not found.' ;;
    # SBM_E_OPEN_HANDLER_NOT_FOUND
    41) error_message='Open Handler Not found.' ;;
    # SBM_E_FINDER_NOT_FOUND
    42) error_message='Finder Not found.' ;;
    # SBM_E_BROWSER_NOT_FOUND
    43) error_message='Web Browser Not found.' ;;
    # SBM_E_BOOKMARK_EXISTS
    50) error_message='Bookmark already exists.' ;;
  esac

  sbm_print \
    "$(sbm_print_red "[Error]      [${error_message}] ${extra_message}")" \
    >&2

  unset error_code error_message extra_message
}

sbm_usage_indentation() {
  num_of_spaces=${1:-2}

  command printf "%${num_of_spaces}s"
  unset num_of_spaces
}

sbm_usage_it_gap() {
  subject="$*"
  subject_length="${#subject}"
  spaces="$((SBM_HELP_TAB_LENGTH - subject_length))"

  if [ 0 -ge "$spaces" ]; then
    command printf "%d" 0
  else
    command printf "%d" "$spaces"
  fi

  unset subject subject_length spaces
}

sbm_print_usage_line() {
  usage_line_item="$(sbm_print_green "$1")"
  usage_line_item_description="$2"
  usage_line_spaces="$(sbm_usage_it_gap "$1")"
  usage_line_total_spaces="$((usage_line_spaces + 1))"
  indentation="$(sbm_usage_indentation $usage_line_total_spaces)"

  command printf \
    "  %s${indentation}%s\n" \
    "$usage_line_item" \
    "$usage_line_item_description"

  unset usage_line_item usage_line_item_description
  unset usage_line_spaces usage_line_total_spaces indentation
}

sbm_print_command_usage_line() {
  item="$(sbm_print_green "$1")"
  item_usage="$2"
  item_description="$3"
  spaces="$(sbm_usage_it_gap "$1 $2")"
  total_spaces="$((spaces + 1))"

  indentation="$(sbm_usage_indentation "$total_spaces")"

  command printf "  %s %s${indentation}%s\n" "$item" "$item_usage" "$item_description"

  unset item item_usage item_description spaces total_spaces indentation
}

sbm_show_version() {
  readonly sbm_version="$(sbm_read_self_doc_value "Version")"
  readonly sbm_name="$(sbm_read_self_doc_value "Name")"
  readonly display_version="$(sbm_print_yellow "$sbm_version")"

  command printf '\n'
  command printf "🔧 %s %s" "$sbm_name" "$display_version"
  command printf '\n'
}

sbm_apply_config_options() {
  if [ -z "${SBM_CONFIG_FILE:-}" ];
  then
    SBM_CONFIG_FILE="${SBM_CONFIG_PATH}/config"
    sbm_print_info "Using Default Bookmarks: $SBM_CONFIG_FILE"
  else
    sbm_print_info "Using Bookmarks Path: $SBM_CONFIG_FILE"
  fi
}

sbm_apply_separator_option() {
  if [ -z "${SBM_OPTION_SEPARATOR:-}" ];
  then
    SBM_OPTION_SEPARATOR='|:|'
    sbm_print_info "Using Default Separator: $SBM_OPTION_SEPARATOR"
  else
    sbm_print_info "Using Separator: $SBM_OPTION_SEPARATOR"
  fi
}

sbm_apply_bookmark_path_options() {
  if [ -z "${SBM_BOOKMARKS_PATH:-}" ];
  then
    sbm_print_error_message "${SBM_E_NOT_FOUND}" 'No bookmarks file given'
    exit "${SBM_E_NOT_FOUND}"
  else
    SBM_BOOKMARKS_PATH="$(sbm_resolve_path "$(sbm_get_path "$SBM_BOOKMARKS_PATH")")"
    [ -z "${SBM_BOOKMARKS:-}" ] \
      && SBM_BOOKMARKS="Primary: ${SBM_BOOKMARKS_PATH}" \
      || SBM_BOOKMARKS="Primary: ${SBM_BOOKMARKS_PATH},${SBM_BOOKMARKS}"

    sbm_print_info "Using Bookmarks Path: $SBM_BOOKMARKS_PATH"
  fi
}

sbm_apply_finder_options() {
  if [ -z "${SBM_OPTION_FINDER:-}" ];
  then
    SBM_OPTION_FINDER="$(command -v rofi)"
    sbm_print_info "Using Default Finder: $SBM_OPTION_FINDER"
  else
    sbm_print_info "Using Finder: $SBM_OPTION_FINDER"
  fi
}

sbm_apply_monitor_options() {
  if [ -z "${SBM_OPTION_MONITOR:-}" ];
  then
    SBM_OPTION_MONITOR=0
    sbm_print_info "Using Default Monitor: $SBM_OPTION_MONITOR"
  else
    sbm_print_info "Using Monitor: $SBM_OPTION_MONITOR"
  fi
}

sbm_apply_browser_options() {
  if [ -z "${SBM_WEB_BROWSER:-}" ];
    then
      SBM_WEB_BROWSER='firefox'
      sbm_print_info "Using Default Browser: $SBM_WEB_BROWSER"
    else
      SBM_WEB_BROWSER="$(sbm_resolve_path "$(sbm_get_path "$SBM_WEB_BROWSER")")"
      [ -z "${SBM_WEB_BROWSERS:-}" ] \
        && SBM_WEB_BROWSERS="Primary: ${SBM_WEB_BROWSER}" \
        || SBM_WEB_BROWSERS="Primary: ${SBM_WEB_BROWSER},${SBM_WEB_BROWSERS}"

      sbm_print_info "Using Browser: $SBM_WEB_BROWSER"
    fi
}

sbm_apply_open_handler_options() {
  if [ -z "${SBM_OPTION_OPEN_HANDLER:-}" ];
  then
    for handler in browser-exec xdg-open cmd.exe cygstart "start" open; do
      if sbm_check_program_exists "$handler";
      then
        if [ "$handler" = "cmd.exe" ];
        then
          SBM_OPTION_OPEN_HANDLER="$handler /c start";
        else
          SBM_OPTION_OPEN_HANDLER="$handler";
        fi
        break;
      fi
    done
    sbm_print_info "Using Default Handler: $SBM_OPTION_OPEN_HANDLER"
  else
    SBM_OPTION_OPEN_HANDLER="$(sbm_resolve_path "$(sbm_get_path "$SBM_OPTION_OPEN_HANDLER")")"
    sbm_print_info "Using Handler: $SBM_OPTION_OPEN_HANDLER"
  fi
}

sbm_apply_command_options() {
  if [ -z "${SBM_OPTION_COMMAND:-}" ];
  then
    sbm_apply_browser_options
    sbm_apply_open_handler_options
  else
    sbm_print_info "Using Command: $SBM_OPTION_COMMAND"
    sbm_print_info "Using Browser: $SBM_OPTION_COMMAND"
    sbm_print_info "Using Handler: $SBM_OPTION_COMMAND"
  fi
}

sbm_apply_options() {
  sbm_apply_config_options
  sbm_apply_separator_option
  sbm_apply_bookmark_path_options
  sbm_apply_finder_options
  sbm_apply_monitor_options
  sbm_apply_command_options
}

sbm_parse_config_file() {
  if [ -z "${SBM_CONFIG_FILE:-}" ] || [ ! -r "$SBM_CONFIG_FILE" ];
  then
    sbm_print_info \
      "No config file or is unreadable: $SBM_CONFIG_FILE"
    return 0
  fi
  sbm_print_info "Reading config file: $SBM_CONFIG_FILE"

  while read -r line;
  do
    case "$line" in
      \#*) continue ;;
      '') continue ;;
    esac

    config_key=${line%%=*}
    config_value=${line##*=}

    case "$config_key" in
      PrimaryBookmark)
        [ -z "${SBM_BOOKMARKS_PATH:-}" ] \
          && SBM_BOOKMARKS_PATH="$config_value"
        ;;
      Bookmark[0-9])
        if [ -z "${SBM_BOOKMARKS:-}" ];
        then
          SBM_BOOKMARKS="$config_value"
        else
          SBM_BOOKMARKS="${SBM_BOOKMARKS},$config_value"
        fi
        ;;
      Command)
        sbm_use_command "$config_value"
        ;;
      Separator)
        [ -z "${SBM_OPTION_SEPARATOR:-}" ] \
          && SBM_OPTION_SEPARATOR="$config_value"
        ;;
      Monitor)
        [ -z "${SBM_OPTION_MONITOR:-}" ] \
          && SBM_OPTION_MONITOR="$config_value" ;;
      PrimaryWebBrowser)
        [ -z "${SBM_WEB_BROWSER:-}" ] \
          && SBM_WEB_BROWSER="$config_value"
        ;;
      WebBrowser[0-9])
        if [ -z "${SBM_WEB_BROWSERS:-}" ];
        then
          SBM_WEB_BROWSERS="$config_value"
        else
          SBM_WEB_BROWSERS="${SBM_WEB_BROWSERS},$config_value"
        fi
        ;;
      Finder)
        [ -z "${SBM_OPTION_FINDER:-}" ] \
          && SBM_OPTION_FINDER="$config_value"
        ;;
      OpenHandler)
        [ -z "${SBM_OPTION_OPEN_HANDLER:-}" ] \
          && SBM_OPTION_OPEN_HANDLER="$config_value"
        ;;
    esac
  done < "$SBM_CONFIG_FILE"
  unset line
}

sbm_check_environment() {
  if [ -n "${VERBOSE+defined}" ];
  then
    readonly SBM_VERBOSE_LEVEL=3
  fi
}

sbm_use_command() {
  SBM_OPTION_COMMAND="$1"
  SBM_OPTION_OPEN_HANDLER="$SBM_OPTION_COMMAND"
  SBM_WEB_BROWSER="$SBM_OPTION_COMMAND"
}

sbm_use_open_handler() {
  if [ -z "${SBM_OPTION_COMMAND:-}" ];
  then
    if sbm_check_program_exists "$1";
      then
        SBM_OPTION_OPEN_HANDLER="$1"
      elif sbm_check_path_exists "$1";
        SBM_OPTION_OPEN_HANDLER="$1"
      then
        sbm_print_error_message "$SBM_E_OPEN_HANDLER_NOT_FOUND" "$1"
        exit "$SBM_E_OPEN_HANDLER_NOT_FOUND"
      fi
  else
    SBM_OPTION_OPEN_HANDLER="$SBM_OPTION_COMMAND"
  fi
}

sbm_use_browser() {
  if [ -z "${SBM_OPTION_COMMAND:-}" ];
  then
    if sbm_check_program_exists "$1";
      then
        SBM_WEB_BROWSER="$1"
      elif sbm_check_path_exists "$1";
        SBM_WEB_BROWSER="$1"
      then
        sbm_print_error_message "$SBM_E_BROWSER_NOT_FOUND" "$1"
        exit "$SBM_E_BROWSER_NOT_FOUND"
      fi
  else
    SBM_WEB_BROWSER="$SBM_OPTION_COMMAND"
  fi
}

sbm_use_finder() {
  case "$1" in
    *dmenu) SBM_OPTION_FINDER='dmenu' ;;
    *fzf) SBM_OPTION_FINDER='fzf' ;;
    *rofi) SBM_OPTION_FINDER='rofi' ;;
    *)
      sbm_print_error_message "$SBM_E_INVALID_ARGUMENTS" \
      "[$1] is not a valid finder option"
      exit "$SBM_E_INVALID_ARGUMENTS"
  esac
}

sbm_prompt_command() {
  case "$SBM_OPTION_FINDER" in
    *dmenu) SBM_PROMPT='dmenu' ;;
    *fzf) SBM_PROMPT='fzf' ;;
    *rofi) SBM_PROMPT="rofi --match fuzzzy -dmenu" ;;
  esac

  sbm_print_line "$SBM_PROMPT"
}

sbm_prompt() {
  case "$SBM_OPTION_FINDER" in
    *dmenu) dmenu -i -p "${2:-Select bookmark}" < "${1}" ;;
    *fzf) fzf < "${1}" ;;
    *rofi) rofi --match fuzzzy -dmenu -i -p "${2:-Select bookmark}" < "${1}" ;;
  esac
}

sbm_prompt_input() {
  case "$SBM_OPTION_FINDER" in
    *dmenu) dmenu -i -p "${1:-Select choice}" ;;
    *rofi) rofi --match fuzzzy -dmenu -i -p "${1:-Select choice}" ;;
    *) sbm_print_error_message "${SBM_E_INVALID_ARGUMENTS}" \
      "[$SBM_OPTION_FINDER] cannot be used for requesting user input"
      exit "${SBM_E_INVALID_ARGUMENTS}"
  esac
}

sbm_parse_base_arguments() {
  verbose_defined="${VERBOSE+defined}"
  SBM_ARGUMENTS=''
  while [ "${1+defined}" ];
  do
    case "$1" in
      -v) [ -n "$verbose_defined" ] || readonly SBM_VERBOSE_LEVEL=1 ;;
      -vv|--verbose) [ -n "$verbose_defined" ] || readonly SBM_VERBOSE_LEVEL=2 ;;
      -V|--version) sbm_show_version; exit 0 ;;
      -h|--help) sbm_show_usage; exit 0 ;;

      -b|--bookmarks) shift "$(($# ? 1 : 0))"; SBM_BOOKMARKS_PATH="$1" ;;
      -c|--config) shift "$(($# ? 1 : 0))"; SBM_CONFIG_FILE="$1" ;;
      -d|--command) shift "$(($# ? 1 : 0))"; sbm_use_command "$1" ;;
      -f|--finder) shift "$(($# ? 1 : 0))"; sbm_use_finder "$1" ;;
      -m|--monitor) shift "$(($# ? 1 : 0))"; SBM_OPTION_MONITOR="$1" ;;
      -o|--handler) shift "$(($# ? 1 : 0))"; sbm_use_open_handler "$1" ;;
      -s|--separator) shift "$(($# ? 1 : 0))"; SBM_OPTION_SEPARATOR="$1" ;;
      -w|--browser) shift "$(($# ? 1 : 0))"; sbm_use_browser "$1" ;;
      -e|--select-browser)
        shift "$(($# ? 1 : 0))"
        export SBM_OPTION_PROMPT_BROWSER=1
        ;;
      -n|--select-handler)
        shift "$(($# ? 1 : 0))"
        export SBM_OPTION_PROMPT_OPEN_HANDLER=1
        ;;
      *) SBM_ARGUMENTS="${SBM_ARGUMENTS},$1"
    esac
    shift "$(($# ? 1 : 0))"
  done
  unset verbose_defined

  if [ -n "${SBM_ARGUMENTS}" ];
  then
    SBM_ARGUMENTS="$(sbm_trim_all "$(sbm_split_argument "$SBM_ARGUMENTS" ',')")"
  fi
}
