# (SBM) Simple Bookmark Manager
SBM is a browser agnostic bookmark manager. It's goal is to be as super simple, fast and easy to use and _eventually_ well documentated.

SBM's code is as generic as possible to help mitigate cross-platform compatibility issues. There is a target and plan to become _eventually_ system agnostic.

## WIP
As this is a WIP this README and more documentation is pending. I have started work on some man pages but not yet completed them. As this a package I use daily I expect this repo to get frequent updates.

## Highlights:

 - Bookmarks files are just **plain text** files with <key\><separator\><values\>.
 - Can handle multiple bookmark files i.e websites, config files, books etc.
 - Comes with builtin support for rofi
     - rofi (-modi and -dmenu)
     - dmenu
     - fzf
 - Set custom finder - default is rofi
 - Set custom open handler - default is system default
 - Override finder, open handler with custom commands
 - Define primary and other secondary web browsers

## Usage instructions

The SBM package includes the following utilities to help manage bookmarks

 - sbm-add (1) - SBM utility to add bookmarks
 - sbm-browsers (1) - SBM utility to list defined browsers
 - sbm-files (1) - SBM utility to list defined bookmark files
 - sbm-init (1) - SBM utility to init SBM config file
 - sbm-menu (1) - SBM utility rofi mode to perform command tasks
 - sbm-remove (1) - SBM utility to remove bookmarks
 - sbm-select (1) - SBM utility to browse and open bookmarks

